<?php
include '../vendor/autoload.php';
use \chaofml\crontab\EventService;

$event = new EventService();
$event->load('tasks.php');
$event->loop();