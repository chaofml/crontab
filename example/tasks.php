<?php
return [
    // ['*/3 13 * * *','\service\Demo::hello',[]],
    ['* * * * *','\chaofml\crontab\Demo::time',[]],
    //['* * * * *','\chaofml\crontab\Demo::log',['hahaha']],
    ['20 9 * * 1-5','\chaofml\crontab\DingService::send',['测试群里面发今天的工作计划呦。']],
    ['00 18 * * 1-5','\chaofml\crontab\DingService::send',['下班了，别忘了打卡、提交今天的工时啊！']],
	['4-59/5 * * * *','\chaofml\crontab\Demo::exit',[]],   //5分钟退出一次，windows可以让代码重新加载
];