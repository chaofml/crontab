<?php
/**
 * User: chaofml
 * Desc: demo示例功能。必须用静态方法方式。
 * Date: 2020年8月20日
 * Time: 16:18 周四
 */

namespace chaofml\crontab;
// include 'CrontabService.php';

class Demo{
    public static function time(){
        $time = date('H:i:s');
        echo "now $time \r\n";
    }
    public static function hello(){
        $time = date('H:i:s');
        echo "hello world $time \r\n";
    }
    public static function log($content){
        $time = date('Y-m-d H:i:s');
        \file_put_contents('log.txt',"$time $content \r\n",FILE_APPEND);
    }
	public static function exit(){
        exit;
    }
}