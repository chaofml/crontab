<?php
/**
 * User: chaofml
 * Desc: 钉钉发送消息相关接口服务
 *       调用：DingService::send('文本内容')
 * Date: 2020年6月30日
 * Time: 15:07 周二
 */

namespace chaofml\crontab;

//钉钉接口数据
define('DING_WEBHOOK','https://oapi.dingtalk.com/robot/send?access_token=XXXXXX');
define('DING_KEYWORD','各位领导：');

class DingService
{
    /**  
    * 发送钉钉消息
    * @return bool
    */ 
    public static function send( $content,$at = [],$isAtAll = false){
        //添加关键字
        $content = DING_KEYWORD.$content;
        $cont = [
            'msgtype'=> 'text',
            'text'=> [
                'content'=> $content,
            ],
            'at'=>[
                'atMobiles'=>$at,
                'isAtAll'=> $isAtAll
            ],
        ];
        $result = self::postJson(DING_WEBHOOK,$cont);
        return $result;
    }
    /**  
    * 发送Link钉钉消息
    */ 
    public static function link( $content,$title, $picUrl,$messageUrl){
        //添加关键字
        $content = DING_KEYWORD.$content;
        $cont = [
            'msgtype'=> 'link',
            'link'=> [
                'text'=> $content,
                'title'=> $title,
                'picUrl'=> $picUrl,
                'messageUrl'=> $messageUrl,
            ],
        ];
        $result = self::postJson(DING_WEBHOOK,$cont);
        return $result;
    }

    /**  
    * 发送Markdown钉钉消息
    */ 
    public static function markdown( $content,$title ='新消息',$at = [],$isAtAll = false ){
        //添加关键字
        $content = DING_KEYWORD.$content;

        $cont = [
            'msgtype'=> 'markdown',
            'markdown'=> [
                'text'=> $content,
                'title'=> $title,
            ],
            'at'=> [
                'atMobiles'=> $at,
                'isAtAll'=> $isAtAll,
            ],
        ];
        $result = self::postJson(DING_WEBHOOK,$cont);
        return $result;
    }

    /**  
    * 发送actioncard钉钉消息
    */ 
    public static function actioncard( $content,$title ='新消息',$at = [],$isAtAll = false ){
        // TODO
    }

    /**
     * 发送json格式的数据到服务器。
     * @return mixed $result 接口调用返回结果。
     */
    public static function postJson($url, array $post = NULL, array $options = array())
    {
        $jsonStr = \json_encode($post);
        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_POSTFIELDS => $jsonStr,
            // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
            CURLOPT_SSL_VERIFYHOST => 0 ,
            CURLOPT_SSL_VERIFYPEER => 0 ,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: ' . strlen(json_encode($post))
            ),
        );
        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}
